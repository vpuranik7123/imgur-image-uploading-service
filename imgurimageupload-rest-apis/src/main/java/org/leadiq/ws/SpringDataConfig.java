package org.leadiq.ws;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.FileSystemConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.igfs.IgfsPath;
import org.apache.ignite.springdata.repository.config.EnableIgniteRepositories;
import org.leadiq.ws.model.Image;
import org.leadiq.ws.model.UploadJob;
import org.leadiq.ws.util.IgfsUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableIgniteRepositories("org.leadiq.ws.repository")
public class SpringDataConfig {

	public static Ignite ignite;
	
	@Bean
	public Ignite igniteInstance() {
		
		IgniteConfiguration cfg = new IgniteConfiguration();
		cfg.setIgniteInstanceName("ignite");
		cfg.setPeerClassLoadingEnabled(true);
	
		  CacheConfiguration ccfgImg = new CacheConfiguration("imageCache");
		  CacheConfiguration ccfgJob = new CacheConfiguration("jobCache");
		 
		  // Setting SQL schema for the cache.
		  ccfgImg.setIndexedTypes(String.class, Image.class);
		  ccfgJob.setIndexedTypes(String.class, UploadJob.class);
		  cfg.setCacheConfiguration(new CacheConfiguration[] {
				  ccfgImg,
				  ccfgJob
		  });
	
		FileSystemConfiguration fileSystemCfg = new FileSystemConfiguration();
		fileSystemCfg.setName(Application.FILESYSTEMNAME);
		cfg.setFileSystemConfiguration(fileSystemCfg);
		ignite = Ignition.start(cfg);
		
		Application.fs = ignite.fileSystem(Application.FILESYSTEMNAME);
		Application.igfsImagePath = new IgfsPath(Application.IMAGEPATH);
		IgfsUtil.delete(Application.fs, Application.igfsImagePath);
		IgfsUtil.mkdirs(Application.fs, Application.igfsImagePath);
		IgfsUtil.printInfo(Application.fs, Application.igfsImagePath);
		
		return ignite;
		
	}
	
	

	

}
