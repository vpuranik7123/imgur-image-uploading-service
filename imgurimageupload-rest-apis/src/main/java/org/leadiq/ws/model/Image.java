package org.leadiq.ws.model;

import java.io.Serializable;

import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 * Model Object for Image
 * 
 * @author Vinay Puranik
 *
 */
public class Image implements Serializable  {
	
	private static final long serialVersionUID = -0x4A41D2BC10FC4581L;

	
	@QuerySqlField(index = true)
	private String id;
	@QuerySqlField
	private String jobId;
	@QuerySqlField
	private String url;
	@QuerySqlField
	private String filename;
	@QuerySqlField
	private String downloadStatus;
	@QuerySqlField
	private String uploadStatus;
	@QuerySqlField
	private String imgurImageEndpoint;
	@QuerySqlField
	private String igfsPath;
	
	
	public String  getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getIgfsPath() {
		return igfsPath;
	}
	public void setIgfsPath(String igfsPath) {
		this.igfsPath = igfsPath;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDownloadStatus() {
		return downloadStatus;
	}
	public void setDownloadStatus(String downloadStatus) {
		this.downloadStatus = downloadStatus;
	}
	public String getUploadStatus() {
		return uploadStatus;
	}
	public void setUploadStatus(String uploadStatus) {
		this.uploadStatus = uploadStatus;
	}
	public String getImgurImageEndpoint() {
		return imgurImageEndpoint;
	}
	public void setImgurImageEndpoint(String imgurImageEndpoint) {
		this.imgurImageEndpoint = imgurImageEndpoint;
	}

	@Override
	public String toString() {
		return "Image [id=" + id + ", jobId=" + jobId + ", url=" + url + ", filename=" + filename + ", downloadStatus="
				+ downloadStatus + ", uploadStatus=" + uploadStatus + ", imgurImageEndpoint=" + imgurImageEndpoint
				+ ", igfsPath=" + igfsPath + "]";
	}
	
}
