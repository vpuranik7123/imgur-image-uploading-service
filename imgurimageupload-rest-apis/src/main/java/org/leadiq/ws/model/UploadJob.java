package org.leadiq.ws.model;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 * 
 * Model object for UploadJob
 * @author Vinay Puranik
 *
 */
public class UploadJob implements Serializable {
	
	
	private static final long serialVersionUID = -0x11A430C4AED78111L;
	

	@QuerySqlField(index=true)
	private String jobid;
	@QuerySqlField(index=true)
    private String created;
	@QuerySqlField(index=true)
    private String status;
	@QuerySqlField(index=true)
    private String finished;
	
	private Map<String, Set<String>> uploaded;

 
    public String getJobid() {
		return jobid;
	}
	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public String getCreated() {
		return created;
	}
	public void setCreated(String nowAsISO) {
		this.created = nowAsISO;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFinished() {
		return finished;
	}
	public void setFinished(String finished) {
		this.finished = finished;
	}
	@Override
	public String toString() {
		return "UploadJob [ jobid=" + jobid + ", created=" + created + ", status=" + status
				+ ", finished=" + finished + "]";
	}


	public Map<String, Set<String>> getUploaded() {
		return uploaded;
	}


	public void setUploaded(Map<String, Set<String>> uploaded) {
		this.uploaded = uploaded;
	}

	
	
    

}
