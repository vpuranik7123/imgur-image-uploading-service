package org.leadiq.ws.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Collection;

import org.apache.ignite.IgniteException;
import org.apache.ignite.IgniteFileSystem;
import org.apache.ignite.igfs.IgfsException;
import org.apache.ignite.igfs.IgfsPath;
import org.leadiq.ws.model.Image;


public class IgfsUtil {
	
	 /**
     * Creates directories.
     *
     * @param fs IGFS.
     * @param path Directory path.
     * @throws IgniteException In case of error.
     */
	public static void mkdirs(IgniteFileSystem fs, IgfsPath path) throws IgniteException {
        assert fs != null;
        assert path != null;

        try {
            fs.mkdirs(path);
            System.out.println();
            System.out.println(">>> Created directory: " + path);
        }
        catch (IgfsException e) {
            System.out.println();
            System.out.println(">>> Failed to create a directory [path=" + path + ", msg=" + e.getMessage() + ']');
        }

        System.out.println();
    }
 
 
    /**
     * Prints information for file or directory.
     *
     * @param fs IGFS.
     * @param path File or directory path.
     * @throws IgniteException In case of error.
     */
    public static void printInfo(IgniteFileSystem fs, IgfsPath path) throws IgniteException {
        System.out.println();
        System.out.println("Information for " + path + ": " + fs.info(path));
    }
    
    

    /**
     * Lists files in directory.
     *
     * @param fs IGFS.
     * @param path Directory path.
     * @throws IgniteException In case of error.
     */
    public static void list(IgniteFileSystem fs, IgfsPath path) throws IgniteException {
        assert fs != null;
        assert path != null;
        assert fs.info(path).isDirectory();

        Collection<IgfsPath> files = fs.listPaths(path);

        if (files.isEmpty()) {
            System.out.println();
            System.out.println(">>> No files in directory: " + path);
        }
        else {
            System.out.println();
            System.out.println(">>> List of files in directory: " + path);

            for (IgfsPath f : files)
                System.out.println(">>>     " + f.name());
        }

        System.out.println();
    }

    /**
     * Deletes file or directory. If directory
     * is not empty, it's deleted recursively.
     *
     * @param fs IGFS.
     * @param path File or directory path.
     * @throws IgniteException In case of error.
     */
    public static void delete(IgniteFileSystem fs, IgfsPath path) throws IgniteException {
        assert fs != null;
        assert path != null;

        if (!fs.exists(path)) {
			System.out.println();
			System.out.println(">>> Won't delete file or directory (doesn't exist): " + path);
		} else {
			boolean isFile = fs.info(path).isFile();
			try {
				fs.delete(path, true);
				System.out.println();
				System.out.println(">>> Deleted " + (isFile ? "file" : "directory") + ": " + path);
			} catch (IgfsException e) {
				System.out.println();
				System.out.println(">>> Failed to delete " + (isFile ? "file" : "directory") + " [path=" + path
						+ ", msg=" + e.getMessage() + ']');
			}
		}
    }
    
    
    
    public static void downloadImageFromURL(IgniteFileSystem fs, IgfsPath path, Image img)
            throws IgniteException, IOException {
            assert fs != null;
            assert path != null;
            InputStream inputStream = new URL(img.getUrl()).openStream();
    		byte[] b = new byte[2048];
    		int length;

            try (OutputStream outputStream = fs.create(path, true)) {
                System.out.println();
                System.out.println(">>> Created file: " + path);
                while ((length = inputStream.read(b)) != -1)
					outputStream.write(b, 0, length);
        		System.out.println();
                System.out.println(">>> Wrote data to file: " + path);

                inputStream.close();
                outputStream.close();
            }
            
            System.out.println(">>Image Download Complete");
            
        }


}
