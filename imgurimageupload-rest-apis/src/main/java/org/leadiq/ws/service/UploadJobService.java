package org.leadiq.ws.service;

import java.util.Set;

import org.leadiq.ws.model.UploadJob;

public interface UploadJobService {
	
		void processImages(Set<String> imageUrls, String jobId) throws Exception;

		void saveJobDetailToRepo(Set<String> set, String string) throws Exception;

		UploadJob fetchUploadJobDetails(String jobId) throws Exception;
		
		Set<String> fetchUploadedImageUrls() throws Exception;
}
