package org.leadiq.ws.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.igfs.IgfsPath;
import org.codehaus.jackson.map.ObjectMapper;
import org.leadiq.ws.Application;
import org.leadiq.ws.constant.AppConstants;
import org.leadiq.ws.model.Image;
import org.leadiq.ws.model.ImgurResponse;
import org.leadiq.ws.model.ImgurResponseWrapper;
import org.leadiq.ws.model.UploadJob;
import org.leadiq.ws.rest.JerseyRestClientInvoker;
import org.leadiq.ws.util.IgfsUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UploadJobServiceImpl implements UploadJobService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static IgniteCache<String, Image> imageCache;
	private static IgniteCache<String, UploadJob> jobCache;

	@Autowired
	Ignite ignite;

	
	@Override
	public void processImages(Set<String> imageUrls, String jobId) throws Exception {

		logger.info(" > processImages ");
		
		imageCache = ignite.getOrCreateCache("imageCache");
		jobCache = ignite.getOrCreateCache("jobCache");
		
		saveJobDetailToRepo(imageUrls, jobId);
		updateJobStatus(jobId, AppConstants.STATUS_PENDING);

		imageUrls.parallelStream().forEach(url -> {
			try {
				process(url, jobId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		updateStatusOfJobWhenComplete(jobId, AppConstants.STATUS_COMPLETE);
		logger.info(" < processImages ");
	}

	/**
	 * Processes image urls --downloads to IGFS and then Uploads to Imgur
	 * 
	 * @param url
	 * @param jobId
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	private void process(String url, String jobId) throws Exception {
		updateJobStatus(jobId, "in-progress");
		
		CompletableFuture.supplyAsync(() -> setImageObject(url, jobId)).thenApply(image -> addImageToCache(image))
				.thenApply(image -> {
					Image img = null;
					try {
						img= download(image);
					} catch (Exception e1) {
						logger.error("Something went wrong while downloading image with URL :" +image.getUrl(),e1);
						e1.printStackTrace();
					}
					return img;
				}).thenApply(image -> {
					Image img = null;
					try {
						img = uploadToImgur(image);
					} catch (Exception e) {
						logger.error("Something went wrong while uploading image from IGFS with URL :" +image.getUrl(),e);
						e.printStackTrace();
					}
					return img;
				});
	}

	/***
	 * Uploads the downloaded image on IGFS to the authorized Imgur account
	 * 
	 * @param image
	 * @return
	 * @throws Exception
	 */
	private Image uploadToImgur(Image image) throws Exception {

		logger.info("> uploadToImgur");
		image.setUploadStatus(AppConstants.STATUS_PENDING);

		try {
			String jsonResp = JerseyRestClientInvoker.invokeImgurUploadAPI(Application.fs,
					new IgfsPath(Application.igfsImagePath, image.getFilename()));
			ObjectMapper objectMapper = new ObjectMapper();
			image.setImgurImageEndpoint(objectMapper.readValue(objectMapper
					.writeValueAsString(objectMapper.readValue(jsonResp, ImgurResponseWrapper.class).getData()).trim(),
					ImgurResponse.class).getLink());
			image.setUploadStatus(AppConstants.STATUS_COMPLETE);

		} catch (Exception e) {
			image.setUploadStatus(AppConstants.STATUS_FAILED);
			e.printStackTrace();
			throw e;
		}
		updateImageUploadStatusInCache(image);
		logger.info("< uploadToImgur");
		return image;
	}

	/**
	 * Sets image object with metadata
	 * 
	 * @param url
	 * @param jobId
	 * @return
	 */

	private Image setImageObject(String url, String jobId) {
		Image img = new Image();
		img.setUrl(url);
		img.setJobId(jobId);
		img.setFilename(url.substring(url.lastIndexOf("/") + 1));
		img.setDownloadStatus(AppConstants.STATUS_PENDING);
		img.setUploadStatus(AppConstants.STATUS_PENDING);
		return img;
	}

	/**
	 * Adds Image object to the cache
	 * 
	 * @param img
	 * @return
	 */

	private Image addImageToCache(Image img) {
		img.setId(UUID.randomUUID().toString());
		imageCache.put(img.getId(), img);
		return img;
	}

	/***
	 * 
	 * Downloads the image from the given URL and stores it in IGFS
	 * 
	 * @param img
	 * @return
	 */

	private Image download(Image img) throws Exception{
		img.setDownloadStatus("in-progress");
		try {
			IgfsUtil.downloadImageFromURL(Application.fs, new IgfsPath(Application.igfsImagePath, img.getFilename()),
					img);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			img.setDownloadStatus(AppConstants.STATUS_FAILED);
			img.setUploadStatus(AppConstants.STATUS_FAILED);
			throw e;
		}
		return img;

	}

	/**
	 * 
	 * Adds Upload Job Object to the cache (job repository).
	 * 
	 */
	@Override
	public void saveJobDetailToRepo(Set<String> imageUrls, String jobId) throws Exception {

		logger.info("> saveJobDetailToRepo");
		UploadJob job = new UploadJob();
		job.setJobid(jobId);
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		df.setTimeZone(tz);
		job.setCreated(df.format(new Date()));
		job.setFinished(null);
		job.setStatus("in-progress");
		jobCache.put(jobId, job);
		logger.info("< saveJobDetailToRepo");
	}

	/**
	 * 
	 * Based on the jobId returns the status of the Job along with image status
	 * 
	 */
	public UploadJob fetchUploadJobDetails(String jobId)  throws Exception {

		logger.info("> fetchUploadJobDetails");
		UploadJob job = new UploadJob();
		
		QueryCursor<List<?>> cursor = jobCache.query(new SqlFieldsQuery(AppConstants.FETCH_JOB_DETAILS).setArgs(jobId));

		for (List<?> row : cursor) {
			job.setJobid(row.get(0).toString());
			job.setCreated(row.get(1).toString());
			if (row.get(2) != null) {
				job.setFinished(row.get(2).toString());
			}
			job.setStatus(row.get(3).toString());
		}

		Map<String, Set<String>> uploaded = new HashMap<String, Set<String>>();

		Set<String> uploadCompleteUrls = new HashSet<>();
		Set<String> uploadPendingUrls = new HashSet<>();
		Set<String> uploadFailedUrls = new HashSet<>();

		QueryCursor<List<?>> images = imageCache
				.query(new SqlFieldsQuery(AppConstants.FETCH_IMAGES_ASSOCIATED_WITH_JOB).setArgs(jobId));

		for (List<?> row : images) {
			if (AppConstants.STATUS_COMPLETE.equals(row.get(2).toString())) {
				uploadCompleteUrls.add(row.get(1).toString());
			} else if (AppConstants.STATUS_PENDING.equals(row.get(2).toString())) {
				uploadPendingUrls.add(row.get(0).toString());
			} else if (AppConstants.STATUS_FAILED.equals(row.get(2).toString())) {
				uploadFailedUrls.add(row.get(0).toString());
			}

		}

		uploaded.put(AppConstants.STATUS_PENDING, uploadPendingUrls);
		uploaded.put(AppConstants.STATUS_COMPLETE, uploadCompleteUrls);
		uploaded.put(AppConstants.STATUS_FAILED, uploadFailedUrls);

		job.setUploaded(uploaded);
		logger.info("< fetchUploadJobDetails");
		return job;
	}

	public void updateStatusOfJobWhenComplete(String jobId, String status) throws Exception {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		df.setTimeZone(tz);
		jobCache.query(new SqlFieldsQuery(AppConstants.UPDATE_JOB_FIELDS_POST_COMPLETION).setArgs(status,
				df.format(new Date()), jobId));

	}

	public void updateJobStatus(String jobId, String status) throws Exception {
		jobCache.query(new SqlFieldsQuery(AppConstants.UPDATE_JOB_STATUS).setArgs(status, jobId));

	}

	public void updateImageUploadStatusInCache(Image img) throws Exception {
		imageCache.query(new SqlFieldsQuery(AppConstants.UPDATE_IMAGE_FIELDS).setArgs(img.getUploadStatus(),
				img.getImgurImageEndpoint(), img.getId()));

	}

	/**
	 * Returns imgur image endpoints of successfully uploaded images
	 * 
	 */
	public Set<String> fetchUploadedImageUrls() throws Exception {

		Set<String> uploadedUrls = new HashSet<>();
		QueryCursor<List<?>> imgurEndPoints = imageCache
				.query(new SqlFieldsQuery(AppConstants.FETCH_UPLOADED_IMAGES).setArgs(AppConstants.STATUS_COMPLETE));
		for (List<?> row : imgurEndPoints) {
			if (row.get(0) != null)
				uploadedUrls.add(row.get(0).toString());
		}
		return uploadedUrls;
	}

}
