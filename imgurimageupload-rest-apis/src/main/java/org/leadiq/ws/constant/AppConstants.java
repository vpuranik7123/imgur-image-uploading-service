package org.leadiq.ws.constant;

public class AppConstants {
	
	public static final String FETCH_JOB_DETAILS = "select jobid,created,finished,status from UploadJob where jobId = ?";
	public static final String FETCH_IMAGES_ASSOCIATED_WITH_JOB = "select url, imgurImageEndpoint, uploadStatus from Image where jobId = ?";
	public static final String UPDATE_JOB_FIELDS_POST_COMPLETION = "update UploadJob SET status = ?, finished=? where jobId = ?";
	public static final String UPDATE_JOB_STATUS="update UploadJob SET status = ? where jobId = ?";
	public static final String UPDATE_IMAGE_FIELDS = "update Image SET uploadStatus = ?, imgurImageEndpoint = ? where id = ?";
	public static final String FETCH_UPLOADED_IMAGES = "select imgurImageEndpoint from Image where uploadStatus = ?";
	public static final String STATUS_COMPLETE = "complete";
	public static final String STATUS_FAILED = "failed";
	public static final String STATUS_PENDING = "pending";
	public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss:sss'Z'";
	
	
}

