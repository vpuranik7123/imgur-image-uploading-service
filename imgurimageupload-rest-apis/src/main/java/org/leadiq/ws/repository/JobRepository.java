package org.leadiq.ws.repository;

import org.apache.ignite.springdata.repository.IgniteRepository;
import org.apache.ignite.springdata.repository.config.RepositoryConfig;
import org.leadiq.ws.model.UploadJob;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
@RepositoryConfig(cacheName = "jobCache")
public interface JobRepository extends IgniteRepository<UploadJob, String> {
	

}
