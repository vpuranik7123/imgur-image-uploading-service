package org.leadiq.ws.repository;

import org.apache.ignite.springdata.repository.IgniteRepository;
import org.apache.ignite.springdata.repository.config.RepositoryConfig;
import org.leadiq.ws.model.Image;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
@RepositoryConfig(cacheName = "imageCache")
public interface ImageRepository extends IgniteRepository<Image, String> {
	


}
