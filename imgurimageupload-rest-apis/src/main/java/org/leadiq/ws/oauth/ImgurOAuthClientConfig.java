package org.leadiq.ws.oauth;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;

import org.apache.commons.httpclient.NameValuePair;
import org.leadiq.ws.Application;
import org.leadiq.ws.rest.JerseyRestClientInvoker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.filter.CompositeFilter;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

@EnableOAuth2Client
@RestController
@Configuration
public class ImgurOAuthClientConfig extends WebSecurityConfigurerAdapter  {
	

	@Autowired
	OAuth2ClientContext oauth2ClientContext;
   

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.antMatcher("/**").addFilterBefore(ssoFilter(), SecurityContextPersistenceFilter.class).authorizeRequests()
				.antMatchers("/").permitAll().anyRequest().authenticated().and().csrf().disable();
	}

	@Override
	public void configure(WebSecurity s) throws Exception {
		s.ignoring().antMatchers("/v1/images/upload", "/imgur", "/v1/images","/v1/images/upload/**");
	}

	private Filter ssoFilter() {

		CompositeFilter filter = new CompositeFilter();
		List<Filter> filters = new ArrayList<>();
		OAuth2ClientAuthenticationProcessingFilter imgurFilter = new OAuth2ClientAuthenticationProcessingFilter(
				"/imgurSSO");
		OAuth2RestTemplate imgurTemplate = new OAuth2RestTemplate(imgur(), oauth2ClientContext);
		imgurFilter.setRestTemplate(imgurTemplate);
		UserInfoTokenServices tokenServices = new UserInfoTokenServices(imgurResource().getUserInfoUri(),
				imgur().getClientId());
		tokenServices.setRestTemplate(imgurTemplate);
		imgurFilter.setTokenServices(tokenServices);
		filters.add(imgurFilter);
		filter.setFilters(filters);
		return filter;
	}

	
	@RequestMapping(value = "/imgur", method = RequestMethod.GET)
	public String ImgurAuth() throws JsonSyntaxException, Exception {

		String authCOde = oauth2ClientContext.getAccessTokenRequest().getAuthorizationCode();
		List<NameValuePair> paramList = new ArrayList<NameValuePair>();

		paramList.add(new NameValuePair("client_id", imgur().getClientId()));
		paramList.add(new NameValuePair("client_secret", imgur().getClientSecret()));
		paramList.add(new NameValuePair("grant_type", imgur().getGrantType()));
		paramList.add(new NameValuePair("code", authCOde));
		Application.imgurAuth = new Gson().fromJson(
				JerseyRestClientInvoker.invokeRestResourcePost(imgur().getAccessTokenUri(), paramList),
				ImgurAuth.class);
		return "Logged In successfully! Username: " + Application.imgurAuth.getUserName();
	}

	@Bean
	@ConfigurationProperties("imgur.client")
	public AuthorizationCodeResourceDetails imgur() {

		AuthorizationCodeResourceDetails authorizationCodeResourceDetails = new AuthorizationCodeResourceDetails();
		authorizationCodeResourceDetails.setAccessTokenUri("https://api.imgur.com/oauth2/token");
		authorizationCodeResourceDetails.setUserAuthorizationUri("https://api.imgur.com/oauth2/authorize");
		authorizationCodeResourceDetails.setGrantType("authorization_code");
		authorizationCodeResourceDetails.setUseCurrentUri(false);
		return authorizationCodeResourceDetails;
	}

	
	@Bean
	@ConfigurationProperties("imgur.resource")
	public ResourceServerProperties imgurResource() {
		return new ResourceServerProperties();
	}
}
