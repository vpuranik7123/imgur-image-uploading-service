package org.leadiq.ws.api;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.leadiq.ws.model.UploadJob;
import org.leadiq.ws.service.UploadJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The ImageController class is a RESTful web service controller. 
 * The <code>@RestController</code> annotation informs Spring that each <code>@RequestMapping</code> 
 * method returns a <code>@ResponseBody</code>which, by default, contains a ResponseEntity converted 
 * into JSON with an associated HTTP status code.
 */
@EnableOAuth2Client
@RestController
public class ImagesController extends BaseController {
	@Autowired
	UploadJobService uploadJobService;

	/**
	* Web service endpoint to create a Upload Job entity.  The HTTP request body is expected to contain a Image URLs in JSON format.
	* @return  A ResponseEntity containing a jobId, if created successfully, and a HTTP status code as described in the method comment.
	*/
 
	@RequestMapping(value = "/v1/images/upload", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, String>> uploadImages(@RequestBody Map<String, Set<String>> urls) {
		logger.info("> uploadImages");
		

		Map<String, String> job = new HashMap<>();
		
		if (urls == null || urls.isEmpty()) {
			job.put("error", "Bad Request, follow the proper syntax");
			return new ResponseEntity<Map<String,String>>(job, HttpStatus.BAD_REQUEST);
		}
		
		if(urls.get("urls").isEmpty()) {
			job.put("error", "Bad Request, Atleast one url of the image should be passed in the request body");
			return new ResponseEntity<Map<String,String>>(job,HttpStatus.BAD_REQUEST);
		}
		
		UUID uuid = UUID.randomUUID();
		logger.info("JobId: " + uuid.toString());
		CompletableFuture.supplyAsync(() -> {
			try {
				uploadJobService.processImages(urls.get("urls"), uuid.toString());
			} catch (Exception e) {
				logger.error("A problem occurred during processing images.", e);
				return new ResponseEntity<Map<String, String>>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return "process images";
		});
		logger.info("Total Image URLs excluding duplicates in the current job:" + urls.get("urls").size());
		
		job.put("jobId", uuid.toString());
		logger.info("< uploadImages");
		return new ResponseEntity<Map<String, String>>(job, HttpStatus.CREATED);
	}

	/**
	* Web service endpoint to that fetches the status of the jobId.  At any given time, for a given jobId, the api returns the status of the image upload associated with the job.
	* @return  A ResponseEntity containing a UploadJob object containing job status info.
	*/
	@RequestMapping(value = "/v1/images/upload/{jobId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UploadJob> getJobStatus(@PathVariable("jobId") String jobId) {
		logger.info("> getJobStatus");
		
		if(jobId == null || jobId == "") {
			logger.error("Job Id is required to fetch the job details.");
			return new ResponseEntity<UploadJob>(HttpStatus.BAD_REQUEST);
		}
		
		UploadJob job = null;
		try {
			job = uploadJobService.fetchUploadJobDetails(jobId);
		} catch (Exception e) {
			logger.error("A problem occurred while fetching jobDetails.", e);
			return new ResponseEntity<UploadJob>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("< getJobStatus");
		return new ResponseEntity<UploadJob>(job, HttpStatus.OK);
	}

	/**
	* Web service endpoint to that returns imgur endpoint url of successfully uploaded images. 
	* @return  imgur endpoints of images
	*/
	@RequestMapping(value = "/v1/images", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Set<String>>> getUploadedImages() {
		logger.info("> getUploadedImages");
		Map<String, Set<String>> uploadedImages = new HashMap<>();
		try {
			uploadedImages.put("uploaded", uploadJobService.fetchUploadedImageUrls());
		} catch (Exception e) {
			logger.error("A problem occurred while fetching uploaded Images.", e);
			return new ResponseEntity<Map<String, Set<String>>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("< getUploadedImages");
		return new ResponseEntity<Map<String, Set<String>>>(uploadedImages, HttpStatus.OK);
	}
}