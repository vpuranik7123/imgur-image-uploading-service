package org.leadiq.ws;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteFileSystem;
import org.apache.ignite.igfs.IgfsPath;
import org.leadiq.ws.oauth.ImgurAuth;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot main ApplicationServes as both the runtime application entry
 * point and the central Java configuration class.
 *
 * @author Vinay Puranik
 *
 */
@SpringBootApplication
public class Application  {


	public static IgniteFileSystem fs;
	public static Ignite ignite;
	public static IgfsPath igfsImagePath;
	public static final String FILESYSTEMNAME = "igfs";
	public static final String IMAGEPATH = "/images";
	public static ImgurAuth imgurAuth;
	

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
	
}