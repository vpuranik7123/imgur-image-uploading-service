
package org.leadiq.ws.rest;


import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.ignite.IgniteFileSystem;
import org.apache.ignite.igfs.IgfsInputStream;
import org.apache.ignite.igfs.IgfsPath;
import org.leadiq.ws.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.impl.MultiPartWriter;

public class JerseyRestClientInvoker  {

	
	protected static Logger logger = LoggerFactory.getLogger(JerseyRestClientInvoker.class);

		public static String invokeRestResourcePost(String url, List<NameValuePair> parameters) throws Exception{
		WebResource webResource = Client.create().resource(url);
		MultivaluedMap<String, String> map = new MultivaluedMapImpl();
		for(NameValuePair nameValuePair : parameters) {
			map.add(nameValuePair.getName(), nameValuePair.getValue());
		}
		String output = null;
		try {
			output = webResource.accept("application/json").type("application/x-www-form-urlencoded").post(ClientResponse.class, map).getEntity(String.class);
			System.out.println("****************  ****  *****-->  InvokeRestResource o/p-- > " +output);
		}catch(Exception e) {
			logger.error("Exception occured while invoking the Rest URL" +e);
			throw e;
		}
		return output;
	}		


		
		public static String invokeImgurUploadAPI(IgniteFileSystem fs, IgfsPath path) throws Exception {
		    IgfsInputStream stream = fs.open(path);
		    FormDataMultiPart part = new FormDataMultiPart().field("image", stream, MediaType.TEXT_PLAIN_TYPE);
		    ClientConfig cc = new DefaultClientConfig();
		    Client client;
		    String response = null;
		    try {
		    	cc.getClasses().add(MultiPartWriter.class);
		    	client = Client.create(cc);
		    	response = client.resource("https://api.imgur.com/3/image").type(MediaType.MULTIPART_FORM_DATA_TYPE).header("Authorization", "Bearer " + Application.imgurAuth.getAccessToken()).post(String.class, part);
		    	System.out.println("****************  ****  *****-->  invokeImgurUploadAPI o/p-- > " +response);
		    }catch(Exception e) {
		    	logger.error("Exception occured while uploading images to Imgur " +e);
		    	throw e;
		    }
		    return response;
		
		}
		
		
	
}