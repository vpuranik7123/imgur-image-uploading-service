Imgur Image Upload APIs: (refer the snapshots provided)
-----------------------

Step 1: 
------
Register a imgur client with the call back url , Specifically, http://server:port/imgur. If you're running the app on local with port 8080 then, your call back url would be http://localhost:8080/imgur

Step 2:
------
Key in the client ID and client secret generated in Step 1 in the application.yml file.

Step 3:
-------
Build and run the app. Once the Spring Boot application is started. Hit the base url : http://localhost:8080/ and Login to Imgur. (this is mandatory)

Step 4:
-------
Once successfully logged in, you are ready to test following Imgur Image Upload APIs. Use any Rest clients (preferably Postman) to test.

    POST /v1/images/upload
    GET /v1/images/upload/:jobId
    GET /v1/images

